#pragma once

#include <vector>
#include <mutex>

template<class T>
struct Optional {
    bool has_value;
    T value;

    Optional() {
        has_value = false;
    }

    Optional(bool has_value, const T& value): has_value(has_value), value(value) {}
};

template<class K, class V>
class ConcurrentHashMap {
public:
    ConcurrentHashMap(size_t max_size, K default_key)
    {
        default_key_ = default_key;
    }

    ConcurrentHashMap(size_t max_size, K default_key, K erased_key): ConcurrentHashMap(max_size, default_key) {
        erased_key_ = Optional<K>{true, erased_key};
    }

    void insert(K key, const V& value) {
    }

    std::pair<bool, V> find(K key) const {
        return std::make_pair(false, V());
    }

    bool erase(K key) {
        return false;
    }

private:
    K default_key_;
    Optional<K> erased_key_;
};
