#pragma once

#include <stack>
#include <mutex>

#include <hazard_ptr.h>

template <class T>
class Stack {
public:
    void Push(const T& value) {
        std::lock_guard<std::mutex> guard(lock_);
        data_.push(value);
    }

    bool Pop(T* value) {
        std::lock_guard<std::mutex> guard(lock_);
        if (data_.empty()) {
            return false;
        }
        *value = std::move(data_.top());
        data_.pop();
        return true;
    }

    void Clear() {
        std::lock_guard<std::mutex> guard(lock_);
        while (!data_.empty()) {
            data_.pop();
        }
    }

private:
    std::stack<T> data_;
    mutable std::mutex lock_;
};
